from FilmRecommendations.MathFunctions import Recommendations

if __name__ == '__main__':
    recommendations=Recommendations()
    critics=recommendations.critics

    #miara prawdopodobienstwa miedzy Lisy i Gena(pitagoras)
    print(recommendations.sim_distance(critics,'Lisa Rose','Gene Seymour'))

    #miara prawdopodobienstwa miedzy Lisy i Gena(pearson)
    print(recommendations.sim_pearson(critics,'Lisa Rose','Gene Seymour'))

    #krytycy podobni do mnie
    print(recommendations.topMatches(critics,'Mateusz', n=5,similarity=recommendations.sim_pearson))

    #jaki film powiniennem zobaczyc kolejny na podstawie podobienstwa krytykow
    print(recommendations.getRecommendations(critics, 'Mateusz',similarity=recommendations.sim_pearson))

    #transformacja film: {oceny...}
    movies = recommendations.transformPrefs(critics)

    #filmy podobne do filmu superman
#KORELACJA
    print(recommendations.topMatches(movies, 'Superman: Powrót',n=5,similarity=recommendations.sim_pearson))

    #rekomendowani krytycy dla filmu (na premierze dadza lepsze oceny)
    #
    print(recommendations.getRecommendations(movies,'Caïe szczÚĂcie',similarity=recommendations.sim_pearson))


