from math import sqrt,pow

class Recommendations():

    critics={
    'Lisa Rose': {'Kobieta w bïÚkitnej wodzie': 2.5, 'WÚĝe w samolocie': 3.5,'Caïe szczÚĂcie': 3.0, 'Superman: Powrót': 3.5, 'Ja, ty i on': 2.5,'Nocny sïuchacz': 3.0},
    'Gene Seymour': {'Kobieta w bïÚkitnej wodzie': 3.0, 'WÚĝe w samolocie': 3.5,'Caïe szczÚĂcie': 1.5, 'Superman: Powrót': 5.0, 'Nocny sïuchacz': 3.0,'Ja, ty i on': 3.5},
    'Michael Phillips': {'Kobieta w bïÚkitnej wodzie': 2.5, 'WÚĝe w samolocie': 3.0,'Superman: Powrót': 3.5, 'Nocny sïuchacz': 4.0},
    'Claudia Puig': {'WÚĝe w samolocie': 3.5, 'Caïe szczÚĂcie': 3.0,'Nocny sïuchacz': 4.5, 'Superman: Powrót': 4.0,'Ja, ty i on': 2.5},
    'Mick LaSalle': {'Kobieta w bïÚkitnej wodzie': 3.0, 'WÚĝe w samolocie': 4.0,'Caïe szczÚĂcie': 2.0, 'Superman: Powrót': 3.0, 'Nocny sïuchacz': 3.0,'Ja, ty i on': 2.0},
    'Jack Matthews': {'Kobieta w bïÚkitnej wodzie': 3.0, 'WÚĝe w samolocie': 4.0,'Nocny sïuchacz': 3.0, 'Superman: Powrót': 5.0, 'Ja, ty i on': 3.5},
    'Toby': {'WÚĝe w samolocie':4.5,'Ja, ty i on':1.0,'Superman: Powrót':4.0},
    'Mateusz' : {'WÚĝe w samolocie': 1.0,'Caïe szczÚĂcie': 3.0, 'Superman: Powrót': 4.5, 'Ja, ty i on': 3.0}
    }

    # zwracanie miary podobieĔstwa opartej na odlegáoĞci dla pozycji person1 i person2
    def sim_distance(self,prefs,person1,person2):
        # pobieranie listy wspólnych pozycji
        si={}
        for item in prefs[person1]:
            if item in prefs[person2]:
                si[item] = 1
            # W przypadku braku wspólnych ocen zostanie zwrócona wartoĞü 0.
        if len(si) == 0: return 0
         # sumowanie kwadratów wszystkich róĪnic
        sum_of_squares = sum([pow(prefs[person1][item] - prefs[person2][item], 2)
                                  for item in prefs[person1] if item in prefs[person2]])
        return 1 / (1 + sum_of_squares)

    # zwracanie wspóáczynnika korelacji Pearsona dla pozycji p1 i p2
    def sim_pearson(self,prefs,p1,p2):
        # pobranie listy wzajemnie ocenianych pozycji
        si={}
        for item in prefs[p1]:
            if item in prefs[p2]: si[item]=1
        # znalezienie liczby elementów
        n = len(si)
        # W przypadku braku wspólnych ocen zostanie zwrócona wartoĞü 0.
        if n == 0: return 0
        # sumowanie wszystkich preferencji
        sum1 = sum([prefs[p1][it] for it in si])
        sum2 = sum([prefs[p2][it] for it in si])
        # sumowanie potĊg
        sum1Sq = sum([pow(prefs[p1][it], 2) for it in si])
        sum2Sq = sum([pow(prefs[p2][it], 2) for it in si])
        # sumowanie iloczynów
        pSum = sum([prefs[p1][it] * prefs[p2][it] for it in si])
        # obliczanie miary korelacji Pearsona
        num = pSum - (sum1 * sum2 / n)
        den = sqrt((sum1Sq - pow(sum1, 2) / n) * (sum2Sq - pow(sum2, 2) / n))
        if den == 0: return 0
        r = num / den
        return r

    # Zwraca najlepsze dopasowania dla osoby ze sáownika prefs.
    # liczba wyników i funkcja podobieĔstwa to opcjonalne parametry
    def topMatches(self,prefs,person,n=5,similarity=sim_pearson):
        scores=[(similarity(prefs,person,other),other) for other in prefs if other!=person]
            # sortowanie listy tak, aby najwyĪsze oceny znalazáy siĊ na samej górze
        scores.sort()
        scores.reverse()
        return scores[0:n]

    # uzyskiwanie rekomendacji dla osoby przy uĪyciu Ğredniej waĪonej
    # rankingów wszystkich innych uĪytkowników
    def getRecommendations(self,prefs,person,similarity=sim_pearson):
        totals={}
        simSums={}
        for other in prefs:
            # zapobieganie porównaniu mnie z samym sobą
            if other==person: continue
            sim=similarity(prefs,person,other)
            # ignorowanie ocen o wartoĞci zerowej lub niĪszej
            if sim<=0: continue
            for item in prefs[other]:
                # Oceniane są tylko te filmy, których jeszcze nie widziaáem.
                if item not in prefs[person] or prefs[person][item]==0:
                    # miara podobieĔstwa * ocena filmu
                    totals.setdefault(item, 0)
                    totals[item] += prefs[other][item] * sim
                    # suma miar podobieĔstwa
                    simSums.setdefault(item, 0)
                    simSums[item] += sim
        # tworzenie listy znormalizowanej
        rankings = [(total / simSums[item], item) for item, total in totals.items()]
        # zwrócenie posortowanej listy
        rankings.sort()
        rankings.reverse()
        return rankings

    def transformPrefs(self,prefs):
        result={}
        for person in prefs:
            for item in prefs[person]:
                result.setdefault(item,{})
                # zamiana miejscami pozycji i osoby
                result[item][person]=prefs[person][item]
        return result
